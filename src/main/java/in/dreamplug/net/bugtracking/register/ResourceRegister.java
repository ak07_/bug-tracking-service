package in.dreamplug.net.bugtracking.register;

import com.google.common.base.Strings;
import com.zaxxer.hikari.HikariDataSource;
import in.dreamplug.net.bugtracking.configuration.BugTrackingConfiguration;
import in.dreamplug.net.bugtracking.configuration.DatabaseConfiguration;
import in.dreamplug.net.bugtracking.health.DatabaseHealthCheck;
import in.dreamplug.net.bugtracking.resource.BugResource;
import in.dreamplug.net.bugtracking.resource.UserResource;
import in.dreamplug.net.bugtracking.service.BugService;
import in.dreamplug.net.bugtracking.service.UserService;
import io.dropwizard.setup.Environment;
import lombok.AllArgsConstructor;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;

@AllArgsConstructor
public class ResourceRegister {
    public void register(final Environment environment, final BugTrackingConfiguration configuration) {
        final HikariDataSource dataSource = createDataSource(environment, configuration.getDatabaseConfiguration());
        final Jdbi jdbi = Jdbi.create(dataSource);
        jdbi.installPlugin(new SqlObjectPlugin());
        final UserService userService = new UserService(jdbi);
        final BugService bugService = new BugService(jdbi);
        environment.jersey().register(new UserResource(userService));
        environment.jersey().register(new BugResource(bugService));
        environment.healthChecks().register("MYSQL", new DatabaseHealthCheck(dataSource));
    }

    private HikariDataSource createDataSource(Environment environment, DatabaseConfiguration databaseConfig) {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl(databaseConfig.getUrl());
        dataSource.setUsername(databaseConfig.getUser());
        dataSource.setPassword(databaseConfig.getPassword());
        dataSource.setMinimumIdle(databaseConfig.getMinPoolSize());
        dataSource.setMaximumPoolSize(databaseConfig.getMaxPoolSize());
        dataSource.setIdleTimeout((long)databaseConfig.getMaxIdleTime());
        dataSource.setMaxLifetime((long)databaseConfig.getMaxLifeTime());
        dataSource.setAutoCommit(databaseConfig.isAutoCommit());
        dataSource.setReadOnly(databaseConfig.isReadOnly());
        dataSource.setConnectionInitSql(databaseConfig.getTestStatement());
        dataSource.setConnectionTimeout((long)databaseConfig.getConnectionTimeout());
        if (!Strings.isNullOrEmpty(databaseConfig.getPoolName())) {
            dataSource.setPoolName(databaseConfig.getPoolName());
        }
        dataSource.setMetricRegistry(environment.metrics());
        return dataSource;
    }
}
