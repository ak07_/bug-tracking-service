package in.dreamplug.net.bugtracking.dao;

import in.dreamplug.net.bugtracking.domain.User;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.Optional;

public interface UserDAO {
    @GetGeneratedKeys
    @SqlUpdate("INSERT INTO USERS (EXTERNAL_ID, NAME, EMAIL, MOBILE_NUMBER, DOB, CREATED_AT, UPDATED_AT) VALUES (:externalId, :name, :email, :mobileNumber, :dob, :createdAt, :updatedAt)")
    Long create(@BindBean User user);

    @SqlUpdate("UPDATE USERS SET NAME = :name, EMAIL = :email, MOBILE_NUMBER = :mobileNumber, DOB = :dob, CREATED_AT = :createdAt, UPDATED_AT = :updatedAt WHERE EXTERNAL_ID = :externalId")
    void update(@BindBean User user);

    @SqlUpdate("DELETE FROM USERS WHERE EXTERNAL_ID = :externalId")
    void deleteById(@Bind("externalId") String externalId);

    @SqlQuery("SELECT * FROM USERS WHERE EXTERNAL_ID = :externalId")
    @RegisterBeanMapper(User.class)
    Optional<User> findById(@Bind("externalId") String externalId);
}
