package in.dreamplug.net.bugtracking.dao;

import in.dreamplug.net.bugtracking.domain.Bug;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface BugDAO {
    @GetGeneratedKeys
    @SqlUpdate("INSERT INTO BUGS (EXTERNAL_ID, PRIORITY, USER_EXTERNAL_ID, DESCRIPTION, CREATED_AT, UPDATED_AT) VALUES (:externalId, :priority, :userExternalId, :description, :createdAt, :updatedAt)")
    Long create(@BindBean Bug bug);

    @SqlUpdate("UPDATE BUGS SET PRIORITY = :priority, USER_EXTERNAL_ID = :userExternalId, DESCRIPTION = :description, CREATED_AT = :createdAt, UPDATED_AT = :updatedAt WHERE EXTERNAL_ID = :externalId")
    void update(@BindBean Bug bug);

    @SqlUpdate("DELETE FROM BUGS WHERE EXTERNAL_ID = :externalId")
    void deleteById(@Bind("externalId") String externalId);

    @SqlQuery("SELECT * FROM BUGS WHERE EXTERNAL_ID = :externalId")
    @RegisterBeanMapper(Bug.class)
    Optional<Bug> findById(@Bind("externalId") String externalId);

    // For paginated bugs retrieval
    @SqlQuery("SELECT * FROM BUGS ORDER BY PRIORITY DESC LIMIT :rowPerPage OFFSET :startRow")
    @RegisterBeanMapper(Bug.class)
    List<Bug> getAllBugsByDescendingPriority(@Bind("startRow") int startRow, @Bind("rowPerPage") int rowPerPage);

    @SqlQuery("SELECT * FROM BUGS WHERE USER_EXTERNAL_ID = :userId ORDER BY PRIORITY DESC")
    @RegisterBeanMapper(Bug.class)
    List<Bug> getAllBugsByUser(@Bind("userId") String userId);

    @SqlUpdate("UPDATE BUGS SET USER_EXTERNAL_ID = 'None', UPDATED_AT = :updatedAt WHERE USER_EXTERNAL_ID = :userExternalId")
    void updateOnUserDelete(@Bind("userExternalId") String userExternalId, @Bind("updatedAt") LocalDateTime updatedAt);
}
