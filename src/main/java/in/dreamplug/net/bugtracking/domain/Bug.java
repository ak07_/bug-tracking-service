package in.dreamplug.net.bugtracking.domain;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "bugs")
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Bug extends ExternalBase {

    @NotNull
    private Integer priority;

    @NotNull
    private String description;

    @NotNull
    private String userExternalId;

    public void update(Bug bug) {
        if(StringUtils.isNotBlank(bug.getUserExternalId())) {
            this.userExternalId = bug.getUserExternalId();
        }
        if(StringUtils.isNotBlank(bug.getDescription())) {
            this.description = bug.getDescription();
        }
        if(bug.getPriority() != null) {
            this.priority = bug.getPriority();
        }
    }
}
