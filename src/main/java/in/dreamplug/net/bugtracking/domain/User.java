package in.dreamplug.net.bugtracking.domain;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "users")
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class User extends ExternalBase{
    @NotNull
    private String name;

    @NotNull
    private String email;

    @NotNull
    private String mobileNumber;

    @NotNull
    private LocalDate dob;

    public void update(User user) {
        if(StringUtils.isNotBlank(user.getName())) {
            this.name = user.getName();
        }
        if(StringUtils.isNotBlank(user.getEmail())) {
            this.email = user.getEmail();
        }
        if(StringUtils.isNotBlank(user.getMobileNumber())) {
            this.mobileNumber = user.getMobileNumber();
        }
        if(user.getDob() != null) {
            this.dob = user.getDob();
        }
    }
}
