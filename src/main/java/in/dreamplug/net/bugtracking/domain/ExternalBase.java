package in.dreamplug.net.bugtracking.domain;

import com.google.common.base.Strings;
import lombok.*;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import java.util.UUID;

@Data
@MappedSuperclass
public class ExternalBase extends Base{
    @Id
    private String externalId;

    public void prePersist() {
        super.prePersist();
        if(Strings.isNullOrEmpty(this.externalId)) {
            this.externalId = UUID.randomUUID().toString();
        }
    }
}
