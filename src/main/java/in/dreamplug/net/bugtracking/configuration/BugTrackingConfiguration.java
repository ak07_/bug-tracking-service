package in.dreamplug.net.bugtracking.configuration;

import io.dropwizard.Configuration;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BugTrackingConfiguration extends Configuration {
    private DatabaseConfiguration databaseConfiguration;
}
