package in.dreamplug.net.bugtracking.resource;

import com.codahale.metrics.annotation.Timed;
import in.dreamplug.net.bugtracking.domain.Bug;
import in.dreamplug.net.bugtracking.service.BugService;
import io.dropwizard.validation.Validated;
import lombok.AllArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

@Path("/bugs")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Timed
@AllArgsConstructor
public class BugResource {
    private BugService bugService;

    @POST
    @Path("/")
    public Response create(final @NotNull @Validated @Valid Bug bug) {
        final Bug _bug = bugService.create(bug);
        return Response.status(Response.Status.CREATED).entity(_bug).build();
    }

    @PATCH
    @Path("/update/{bugId}")
    public Response update(@PathParam("bugId") final String bugId, @NotNull  @Validated Bug bug) {
        bug.setExternalId(bugId);
        final Bug _bug = bugService.update(bug);
        if(_bug == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.status(Response.Status.FOUND).entity(_bug).build();
    }

    @DELETE
    @Path("/delete/{bugId}")
    public Response delete(@PathParam("bugId") final String bugId) {
        final Optional<Bug> _bug = bugService.deleteById(bugId);
        if(_bug.isPresent()) {
            return Response.status(Response.Status.FOUND).entity(_bug.get()).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @GET
    @Path("/get/{bugId}")
    public Response get(@PathParam("bugId") final String bugId) {
        final Optional<Bug> _bug = bugService.findById(bugId);
        if(_bug.isPresent()) {
            return Response.status(Response.Status.FOUND).entity(_bug.get()).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @GET
    @Path("/getAll")
    public Response getAll(@QueryParam("offset") @DefaultValue("-1") int offset, @QueryParam("value") @DefaultValue("-1") int value) {
        offset = offset == -1 ? 0 : offset;
        value = value == -1 ? 10 : value;
        List<Bug> bugs = bugService.getAllBugsByDescendingPriority(offset,value);
        return Response.status(Response.Status.FOUND).entity(bugs).build();
    }

    @GET
    @Path("/getAllByUser/{userId}")
    public Response getAllByUser(@PathParam("userId") final String userId) {
        List<Bug> bugs = bugService.getAllBugsByUser(userId);
        if(bugs.size() == 0) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.status(Response.Status.FOUND).entity(bugs).build();
    }
}
