package in.dreamplug.net.bugtracking.resource;

import com.codahale.metrics.annotation.Timed;
import in.dreamplug.net.bugtracking.domain.User;
import in.dreamplug.net.bugtracking.service.UserService;
import io.dropwizard.validation.Validated;
import lombok.AllArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;

@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Timed
@AllArgsConstructor
public class UserResource {
    private UserService userService;

    @POST
    @Path("/")
    public Response create(final @NotNull @Validated @Valid User user) {
        final User _user = userService.create(user);
        return Response.status(Response.Status.CREATED).entity(_user).build();
    }

    @PATCH
    @Path("/update/{userId}")
    public Response update(@PathParam("userId") final String userId, @NotNull @Validated User user) {
        user.setExternalId(userId);
        final User _user = userService.update(user);
        if(_user == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.status(Response.Status.FOUND).entity(_user).build();
    }

    @DELETE
    @Path("/delete/{userId}")
    public Response delete(@PathParam("userId") final String userId) {
        final Optional<User> _user = userService.deleteById(userId);
        if(_user.isPresent()) {
            return Response.status(Response.Status.FOUND).entity(_user.get()).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @GET
    @Path("/get/{userId}")
    public Response get(@PathParam("userId") final String userId) {
        final Optional<User> _user = userService.findById(userId);
        if(_user.isPresent()) {
            return Response.status(Response.Status.FOUND).entity(_user.get()).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }
}
