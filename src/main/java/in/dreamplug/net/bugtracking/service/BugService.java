package in.dreamplug.net.bugtracking.service;

import in.dreamplug.net.bugtracking.dao.BugDAO;
import in.dreamplug.net.bugtracking.domain.Bug;
import lombok.AllArgsConstructor;
import org.jdbi.v3.core.Jdbi;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
public class BugService {
    private Jdbi jdbi;

    public Bug create(final Bug bug) {
        bug.prePersist();
        final Long _id = jdbi.withExtension(BugDAO.class, dao -> dao.create(bug));
        bug.setId(_id);
        return bug;
    }

    public Bug update(final Bug bug) {
        final Optional<Bug> _bug = findById(bug.getExternalId());
        if(_bug.isPresent()) {
            _bug.get().preUpdate();
            _bug.get().update(bug);
            jdbi.useExtension(BugDAO.class, dao -> dao.update(_bug.get()));
            return _bug.get();
        }
        return null;
    }

    public Optional<Bug> deleteById(final String bugId) {
        final Optional<Bug> _bug = findById(bugId);
        jdbi.useExtension(BugDAO.class, dao -> dao.deleteById(bugId));
        return _bug;
    }

    public Optional<Bug> findById(final String bugId) {
        return jdbi.withExtension(BugDAO.class, dao -> dao.findById(bugId));
    }

    public List<Bug> getAllBugsByDescendingPriority(final int startRow, final int rowPerPage) {
        return jdbi.withExtension(BugDAO.class, dao -> dao.getAllBugsByDescendingPriority(startRow, rowPerPage));
    }

    public List<Bug> getAllBugsByUser(final String userId) {
        return jdbi.withExtension(BugDAO.class, dao -> dao.getAllBugsByUser(userId));
    }
}
