package in.dreamplug.net.bugtracking.service;

import in.dreamplug.net.bugtracking.dao.BugDAO;
import in.dreamplug.net.bugtracking.dao.UserDAO;
import in.dreamplug.net.bugtracking.domain.User;
import lombok.AllArgsConstructor;
import org.jdbi.v3.core.Jdbi;

import java.time.LocalDateTime;
import java.util.Optional;

@AllArgsConstructor
public class UserService {
    private Jdbi jdbi;

    public User create(final User user) {
        user.prePersist();
        final Long _id = jdbi.withExtension(UserDAO.class, dao -> dao.create(user));
        user.setId(_id);
        return user;
    }

    public User update(final User user) {
        final Optional<User> _user = findById(user.getExternalId());
        if(_user.isPresent()) {
            _user.get().preUpdate();
            _user.get().update(user);
            jdbi.useExtension(UserDAO.class, dao -> dao.update(_user.get()));
            return _user.get();
        }
        return null;
    }

    public Optional<User> deleteById(final String userId) {
        final Optional<User> _user = findById(userId);
        jdbi.useExtension(BugDAO.class, dao -> dao.updateOnUserDelete(userId, LocalDateTime.now()));
        jdbi.useExtension(UserDAO.class, dao -> dao.deleteById(userId));
        return _user;
    }

    public Optional<User> findById(final String userId) {
        return jdbi.withExtension(UserDAO.class, dao -> dao.findById(userId));
    }
}
