package in.dreamplug.net.bugtracking;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import in.dreamplug.net.bugtracking.configuration.BugTrackingConfiguration;
import in.dreamplug.net.bugtracking.register.ResourceRegister;
import io.dropwizard.Application;
import io.dropwizard.configuration.ConfigurationSourceProvider;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.jersey.jackson.JsonProcessingExceptionMapper;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class BugTrackingApplication extends Application<BugTrackingConfiguration> {
    public static void main(String[] args) throws Exception {
        new BugTrackingApplication().run(args);
    }

    @Override
    public String getName() {
        return "bug-tracking";
    }

    @Override
    public void initialize(Bootstrap<BugTrackingConfiguration> bootstrap) {
        ConfigurationSourceProvider provider = new SubstitutingSourceProvider(bootstrap.getConfigurationSourceProvider(),
                new EnvironmentVariableSubstitutor(false));
        bootstrap.setConfigurationSourceProvider(provider);
    }

    @Override
    public void run(BugTrackingConfiguration configuration, Environment environment) {
        final ResourceRegister resourceRegister = new ResourceRegister();
        resourceRegister.register(environment, configuration);
        environment.jersey().register(new JsonProcessingExceptionMapper(true));
        environment.getObjectMapper().setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
    }
}
